# PredictMisc
package that do prediction analysis for methylation regions


### Required Packages
Because you must install this package from GitLab, you will need to have a few packages installed on your machine before you can build this package.

- `Aclust`: install this from <https://github.com/tamartsi/Aclust>. This package also suggests the Bioconductor package `AnnotationDbi`.
- `caret`
- `data.table`
- `dplyr`
- `pathwayPCA`: install this from <https://github.com/gabrielodom/pathwayPCA>
- `pROC`
- `tibble`
