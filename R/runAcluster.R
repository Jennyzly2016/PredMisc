#' function that run parallel acluster algorithm given a methylation data frame
#'
#' @param chromeAnnot_df data frame contains 1 chrome annot information for 450k probes
#' @param tBeta_df a long format BetaValue_df to generate aclusters
#' @param minCpGs minimum number of cpgs within each cluster
#' @param ncores number of cores to do parallel computing
#' @return A list of elements containing cg probe names
#'
#' @import Aclust
#' @importFrom data.table as.data.table
#' @importFrom plyr llply
#' @export
#'
#' @examples \dontrun{
#'  data(Example_df)
#'  data(chrome_annot_files)
#'  BetaTrans_df <- TransposeAssay(
#'  Example_df[, -1],
#'  omeNames = "rowNames"
#'  )
#'  runAcluster(
#'     chromeAnnot_df = chrome_annot_files,
#'     tBeta_df = BetaTrans_df,
#'     minCpGs = 5,
#'     ncores = 2
#'  )
#'}
#'


runAcluster <- function(chromeAnnot_df, tBeta_df, minCpGs = 5, ncores = 2) {
  
  if(ncores > 1){
    doParallel::registerDoParallel(cores = ncores)
  }
  aux <- data.table::rbindlist(chromeAnnot_df)
  tBeta_df <- data.matrix(tBeta_df)
  tBeta_df <- tBeta_df[rownames(tBeta_df) %in% aux$IlmnID,]
  aclust.list <- plyr::llply(
    chromeAnnot_df, 
    function(item) {
      suppressMessages({
        library(Aclust)
        library(data.table)
      })
      assign.to.clusters(
        betas = tBeta_df[rownames(tBeta_df) %in% item$IlmnID,],
        annot = as.data.table(item),
        dist.thresh = 0.5,
        bp.merge = 200,
        dist.type = "spearman",
        method = "complete"
      )
    },.progress = "time",.parallel = ifelse(ncores > 1, TRUE, FALSE))
  aa <- unlist(aclust.list, recursive = FALSE)
  aa[lengths(aa) >= minCpGs]
}
