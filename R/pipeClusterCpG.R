#' Evaluate Prediction using both cluster and CpG method
#'
#' @description Read data, based on one row of information_df, then use aclust
#'  methods to select cpgs as predictors fit \link{glmnet} elastic net,
#'  \link{caret} random forest or support vector machine model and evaluate its
#'   prediction performance.
#'
#' @param rowNum num of row in information_df
#' @param Beta_df Beta_df is a data frame that each row is a cpg probe, each col
#'   is a sample id, each cell is a Beta value, first column is the phenodata, please
#'   make sure this column is a factor with levels, or we can not ensure accuracy
#'   of the results
#' @param beta2M whether transfre beta to m value before prediction
#' @param respCol_index response variable col number in beta data frame
#' @param designInfo_df information df generate by \link{summaryInfo} function
#' @param chromeAnnot_ls list that contains 22 chrome annot probe information
#' @param arrayType Type of array, can be "450k" or "EPIC".
#' @param alphaValue vector that storage alpha values
#' @param ncores number of cores to do parallel computing
#' @param chooseCpGs what cpg selection method to use, use full cpgs \link{fullCpGs}
#'        within cluster or PC1 score \link{pc1CpGs} of cluster or maximum \link{maxCpGs}
#'        expression score, default set to fullcpgs, it is feasible to write new methods
#'        by adding a new function that take in train/test dataset and cpglist then
#'        return a list of train and test subset data.
#' @param predictMethod what prediction method to use
#' @param outcome_type type of outcome variable, gauusian or binomial or poisson, etc
#' @param resultPath path to storage results
#' @param save whether to save the results
#'
#' @return return a list with three elements,\cr
#' \enumerate{
#'     \item first element is the fit model results of different prediction methods
#'     \item Second item second element is the data frame that contains evalutation parameters of
#'           different prediction methods' performace:\cr
#'         for glmnet net, the data frame has row number equal to number of alpha
#'           values given in the function argument times 16 columns with different
#'           evaluation parameters including NumOfRep,NumOfCv, auc_results,
#'           Sensitivity, Specificity, etc;\cr
#'         for random forest and support vector machine, the data frame has one
#'           row times 14 columns with different evaluation parameters including
#'           NumOfRep,NumOfCv, auc_results, Sensitivity, Specificity, etc\cr
#'     \item third element is a vector that indicate number of predictors used
#'  }
#' @details
#' \describe{
#'  \item{predictMethod1: }{Elastic net from function \link[glmnet]{glmnet}
#'      to do prediction}
#'  \item{predictMethod2: }{Random Forest from function \link[caret]{train}
#'      to do prediction(requires package "randomForest" installed first)}
#'  \item{predictMethod3: }{Support Vector Machine from function
#'      \link[caret]{train} to do prediction(requires package "kernlab" installed)}
#' }
#'
#' @importFrom pathwayPCA TransposeAssay
#' @importFrom coMethDMR CoMethAllRegions
#'
#' @export
#'
#' @examples \dontrun{
#'  data(Example_df)
#'  data(pfcInfo_df)
#'  
#'  probes.cluster.exp <- list(
#'    chrome_annot_files[[1]]$IlmnID
#'  )
#'
#'  test <- pipeClusterCpG(
#'    rowNum = 10,
#'    Beta_df = Example_df,
#'    beta2M = TRUE,
#'    respCol_index = 1,
#'    designInfo_df = pfcInfo_df,
#'    chromeAnnot_ls = probes.cluster.exp,
#'    alphaValue = seq(0, 1, by = 0.1),
#'    ncores = 2,
#'    chooseCpGs = fullCpGs,
#'    predictMethod = "glmnet",
#'    outcome_type = "binomial",
#'    save = FALSE,
#'    resultPath = NULL
#'  )
#'
#' }
pipeClusterCpG <- function(rowNum, Beta_df, beta2M = TRUE, respCol_index,
                          designInfo_df, chromeAnnot_ls, arrayType = c("450k", "EPIC"),
                          alphaValue = seq(0, 1, by = 0.1), ncores = 2,
                          chooseCpGs = medianCpGs,
                          predictMethod = c("glmnet", "randomForest", "svm"),
                          outcome_type = "binomial",save = FALSE,resultPath = NULL){
  
  # 1. cluster part
  
  ### 1.0. filter Beta_df
  cpgs_in_clusters_chr <- base::intersect(
    unique(unlist(chromeAnnot_ls)),
    colnames(Beta_df)
  )
  
  Beta_cluster_df <- Beta_df[
    , c(respCol_index, match(cpgs_in_clusters_chr, colnames(Beta_df)))
  ]
  
  ### 1.1. divided methylation data into train and test data
  TrainTest <- methSplit(
    rowNum = rowNum,
    Beta_df = Beta_cluster_df,
    designInfo_df = designInfo_df
  )
  BetaPhenoTrain_df <- TrainTest$Train_df
  BetaPhenoTest_df <- TrainTest$Test_df
  
  ### 1.2. Apply CoMethAllRegions() to find DMRs
  tBetaPhenoTrain_df <- TransposeAssay(
    BetaPhenoTrain_df[ , -respCol_index],
    omeNames = "rowNames"
  )
  
  coMeth.list <- CoMethAllRegions(
    dnam = tBetaPhenoTrain_df,
    betaToM = TRUE,
    method = "pearson",
    rDropThresh_num = 0.4,
    minPairwiseCorr = NULL,
    minCpGs = 3,
    arrayType = arrayType,
    CpGs_ls = chromeAnnot_ls,
    file = NULL,
    returnAllCpGs = FALSE,
    output = "CpGs",
    nCores_int = ncores
  )
  names(coMeth.list) <- NULL
  
  ### 1.3. Turn beta values into Mvalues
  trans_list <- beta2M_wrapper(
    beta2M = beta2M,
    BetaPhenoTrain_df = BetaPhenoTrain_df,
    BetaPhenoTest_df = BetaPhenoTest_df,
    respCol_index = respCol_index,
    returnType = "data.frame"
  )
  Train_df <- trans_list$Train
  Test_df <- trans_list$Test
  
  ### 1.4. Summarize DMRs
  sumTnT_ls <- summarizeCpGs(
    clust_ls = coMeth.list,
    train_df = Train_df,
    test_df = Test_df,
    pheno_index = BetaPhenoTrain_df[,respCol_index],
    ncores = ncores,
    selectMethod = chooseCpGs
  )
  Train_sum_df <- sumTnT_ls$train_df
  Test_sum_df <- sumTnT_ls$test_df
  p <- sumTnT_ls$npredictors
  
  PhenoTrain_cluster_df <- cbind(
    BetaPhenoTrain_df[respCol_index],
    Train_sum_df
  )
  PhenoTest_cluster_df <- cbind(
    BetaPhenoTest_df[respCol_index],
    Test_sum_df
  )
  
  # 2. cpg part
  
  ### 2.0. filter Beta_df
  Beta_cpg_df <- Beta_df[
    , !(colnames(Beta_df) %in% cpgs_in_clusters_chr)
  ]
  
  ### 2.1. divided methylation data into train and test data
  TrainTest2 <- methSplit(
    rowNum = rowNum,
    Beta_df = Beta_cpg_df,
    designInfo_df = designInfo_df
  )
  BetaPhenoTrain_df2 <- TrainTest2$Train_df
  BetaPhenoTest_df2 <- TrainTest2$Test_df
  
  ### 2.2. turn beta values into Mvalues
  trans_list2 <- beta2M_wrapper(
    beta2M = beta2M,
    BetaPhenoTrain_df = BetaPhenoTrain_df2,
    BetaPhenoTest_df = BetaPhenoTest_df2,
    respCol_index = respCol_index,
    returnType = "matrix"
  )
  Train_mat <- trans_list2$Train
  Test_mat <- trans_list2$Test
  
  ### 2.3. t test to filter sig cpgs
  diffGroup <- split(
    x = BetaPhenoTrain_df2,
    f = BetaPhenoTrain_df2[, respCol_index]
  )
  group1Name <-  rownames(diffGroup[[1]])
  group2Name <-  rownames(diffGroup[[2]])
  
  group1_mat <- Train_mat[group1Name, ]
  group2_mat <- Train_mat[group2Name, ]
  
  pvalue_vec <- parTtest(group1_mat, group2_mat, ncores = ncores)
  pvalue_vec_sorted <- sort(pvalue_vec)
  pvalue_vec_sorted_sig <- pvalue_vec_sorted[pvalue_vec_sorted < 0.05]
  
  ### 2.4 choose predictors
  
  #### 2.4.1 match npred from cluster method
  predictors <- names(pvalue_vec_sorted_sig)[seq_len(p)]
  
  Train_cpg_df <- as.data.frame(Train_mat[ , predictors])
  Test_cpg_df  <- as.data.frame(Test_mat[ , predictors])
  p2 <- length(predictors)
  
  PhenoTrain_cpg_df <- cbind(
    BetaPhenoTrain_df2[ , respCol_index],
    Train_cpg_df
  )
  PhenoTest_cpg_df <- cbind(
    BetaPhenoTest_df2[ , respCol_index],
    Test_cpg_df
  )
  
  colnames(PhenoTrain_cpg_df)[1] <- colnames(BetaPhenoTrain_df2)[respCol_index]
  colnames(PhenoTest_cpg_df)[1] <- colnames(BetaPhenoTest_df2)[respCol_index]
  
  #### 2.4.2 pick all sig cpgs
  Train_cpg_df2 <- as.data.frame(Train_mat[ , names(pvalue_vec_sorted_sig)])
  Test_cpg_df2  <- as.data.frame(Test_mat[ , names(pvalue_vec_sorted_sig)])
  p3 <- length(names(pvalue_vec_sorted_sig))
  
  PhenoTrain_cpg_df2 <- cbind(
    BetaPhenoTrain_df2[ , respCol_index],
    Train_cpg_df2
  )
  PhenoTest_cpg_df2 <- cbind(
    BetaPhenoTest_df2[ , respCol_index],
    Test_cpg_df2
  )
  
  colnames(PhenoTrain_cpg_df2)[1] <- colnames(BetaPhenoTrain_df2)[respCol_index]
  colnames(PhenoTest_cpg_df2)[1] <- colnames(BetaPhenoTest_df2)[respCol_index]
  
  ### 2.5 prepare datasets
  
  #### 2.5.1 for method 1 -- match npred
  PhenoTrain_df <- merge(
    PhenoTrain_cluster_df, PhenoTrain_cpg_df,
    by = c("row.names", colnames(BetaPhenoTrain_df)[respCol_index])
  )
  row.names(PhenoTrain_df) <- PhenoTrain_df$Row.names
  PhenoTrain_df$Row.names <- NULL
  
  PhenoTest_df <- merge(
    PhenoTest_cluster_df, PhenoTest_cpg_df,
    by = c("row.names", colnames(BetaPhenoTest_df)[respCol_index])
  )
  row.names(PhenoTest_df) <- PhenoTest_df$Row.names
  PhenoTest_df$Row.names <- NULL
  
  #### 2.5.2 for method 2 -- all sig
  PhenoTrain_df2 <- merge(
    PhenoTrain_cluster_df, PhenoTrain_cpg_df2,
    by = c("row.names", colnames(BetaPhenoTrain_df2)[respCol_index])
  )
  row.names(PhenoTrain_df2) <- PhenoTrain_df2$Row.names
  PhenoTrain_df2$Row.names <- NULL
  
  PhenoTest_df2 <- merge(
    PhenoTest_cluster_df, PhenoTest_cpg_df2,
    by = c("row.names", colnames(BetaPhenoTest_df2)[respCol_index])
  )
  row.names(PhenoTest_df2) <- PhenoTest_df2$Row.names
  PhenoTest_df2$Row.names <- NULL
  
  # 3. Fit prediction models
  seed.value <- designInfo_df$seed[rowNum]
  NumOfRep   <- designInfo_df$NumOfRep[rowNum]
  NumOfCv    <- designInfo_df$NumOfCv[rowNum]
  
  ### 3.1 for method 1 -- match npred
  predict_ls <- predict_wrapper(
    predictMethod = predictMethod,
    alphaValue = alphaValue,
    PhenoTrain_df = PhenoTrain_df,
    PhenoTest_df = PhenoTest_df,
    respCol_index = respCol_index,
    outcome_type = outcome_type,
    seed_int = seed.value,
    whichRep_int = NumOfRep,
    whichCVfold_int = NumOfCv,
    ncores = ncores
  )
  
  out_ls <- list(
    Fit = predict_ls$Fit,
    PredPerformance = predict_ls$performance,
    numOfPredictors_cluster = p,
    numOfPredictors_cpg = p2,
    numOfPredictors_all = p+p2
  )
  
  ### 3.2 for method 2 -- all sig
  predict_ls2 <- predict_wrapper(
    predictMethod = predictMethod,
    alphaValue = alphaValue,
    PhenoTrain_df = PhenoTrain_df2,
    PhenoTest_df = PhenoTest_df2,
    respCol_index = respCol_index,
    outcome_type = outcome_type,
    seed_int = seed.value,
    whichRep_int = NumOfRep,
    whichCVfold_int = NumOfCv,
    ncores = ncores
  )
  
  out_ls2 <- list(
    Fit = predict_ls2$Fit,
    PredPerformance = predict_ls2$performance,
    numOfPredictors_cluster = p,
    numOfPredictors_cpg = p3,
    numOfPredictors_all = p+p3
  )
  
  list(
    match_results = out_ls,
    allSig_results = out_ls2
  )
  
}