#' select method that find the most significant expression within a cluster
#'
#' @param clust_ls list of cpgs, each slot contains few cpgs
#' @param train_df data frame that each row is a train samples
#' @param test_df data frame that each row is a test samples
#' @param pheno_index vector of phenotype info
#' @param ncores number of cores to do parallel computing
#' 
#' @return a list contains train data frame and test data frame and number of predictors
#' 
#' @importFrom plyr llply
#' 
#' @export
#'
#' @examples \dontrun{
#'  data(aclust.listDemo)
#'  data(ExampleMvalue_train)
#'  data(ExampleMvalue_test)
#'
#'  test <- mostSigCpGs(
#'    clust_ls = aclust.listDemo,
#'    train_df = ExampleMvalue_train[,-1],
#'    test_df = ExampleMvalue_test[,-1],
#'    pheno_index = as.character(ExampleMvalue_train[,1])
#'  )
#' }
mostSigCpGs <- function(clust_ls, 
                        train_df, 
                        test_df, 
                        pheno_index = NULL, 
                        ncores = 2){
  
  if(ncores > 1){
    doParallel::registerDoParallel(cores = ncores)
  }
  
  res <- plyr::llply(
    .data = clust_ls,
    .fun = function(ele) {
      if (identical(intersect(ele, colnames(train_df)), character(0))) {
        list(
          mostSigTrain_vec = NULL,
          mostSigTest_vec = NULL
        )
      } else {
        Train_df <- train_df[, intersect(ele, colnames(train_df))]
        Test_df  <- test_df[, intersect(ele, colnames(train_df))]
        
        diffGroup <- split(
          x = Train_df,
          f = pheno_index
        )
        
        group1_df <- Train_df[rownames(diffGroup[[1]]), ]
        group2_df <- Train_df[rownames(diffGroup[[2]]), ]
        
        pvalue_vec <- parTtest(group1_df, group2_df, ncores = ncores)
        p.sorted_vec <- sort(pvalue_vec, decreasing = TRUE)
        
        list(
          mostSigTrain_vec = Train_df[, names(p.sorted_vec)[1]],
          mostSigTest_vec = Test_df[, names(p.sorted_vec)[1]]
        )
      }
    },
    .parallel = ifelse(ncores > 1, TRUE, FALSE)
  )
  
  Train_df <- do.call(cbind, lapply(res, `[[`, "mostSigTrain_vec"))
  n <- ncol(Train_df)
  colnames(Train_df) <- paste0("predictor", seq_len(n))
  Test_df <- do.call(cbind, lapply(res, `[[`, "mostSigTest_vec"))
  colnames(Test_df) <- colnames(Train_df)
  
  list(
    train_df = Train_df,
    test_df = Test_df,
    npredictors = n
  )
  
}